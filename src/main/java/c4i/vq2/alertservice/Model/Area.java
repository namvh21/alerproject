package c4i.vq2.alertservice.Model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@ToString
public class Area {
    private UUID areaId ;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSSZ")
    private Timestamp createTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSSZ")
    private Timestamp updateTime;
    private double areaLatitude ;
    private double areaLongitude;
    private String createUser;
    private String updateUser;
    private double areaRadius;
    private String areaName;
}








package c4i.vq2.alertservice.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
public class AlertInfo {
    private UUID areaId ;
    private double distance;

}

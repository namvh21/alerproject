package c4i.vq2.alertservice.Model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Track {
    private int id;
    private String name;
    private int category;
    private double latitude;
    private double longitude;
    private double altitude;
    private double velocity;
    private double direction;
}

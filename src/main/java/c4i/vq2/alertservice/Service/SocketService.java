package c4i.vq2.alertservice.Service;

import c4i.vq2.alertservice.Model.AlertInfo;
import c4i.vq2.alertservice.Model.Track;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.ArrayList;

@Service
public class SocketService {


    @Value("${socket.server}")
    private String socketServer ;
    private Socket socket = IO.socket(URI.create("http://10.61.53.202:6969"));

    @PostConstruct
    private void postConstruct(){
        this.socket = IO.socket(URI.create(socketServer)).connect();
        this.updateTracks();
    }


    public void socketUpdateAlert(AlertInfo alertInfo)  {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try{
            String areaJson = ow.writeValueAsString(alertInfo);
            socket.emit("update-alert",areaJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    public void updateTracks(){
        socket.on("send_tracks", new Emitter.Listener() {
            @SneakyThrows
            @Override
            public void call(Object... objects) {
                JSONArray array = ((JSONArray)objects[0]).getJSONArray(0);
                for(int n = 0; n < array.length(); n++)
                {
                    JSONObject jsonObject = array.getJSONObject(n);
                    Gson gson = new Gson();
                    Track track = gson.fromJson(String.valueOf(jsonObject), Track.class);
                    System.out.println( track.getLongitude());
                }

            }
        });
    }
}

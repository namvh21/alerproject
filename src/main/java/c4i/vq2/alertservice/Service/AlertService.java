package c4i.vq2.alertservice.Service;

import c4i.vq2.alertservice.Model.AlertInfo;
import c4i.vq2.alertservice.Model.Area;
import c4i.vq2.alertservice.Model.Track;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@Getter
@Setter
public class AlertService {
    @Value("${base.uri.area}")
    private String baseUriArea ;
    @Value("${base.uri.track}")
    private String baseUriTrack ;
    @Value("${server.port}")
    private  int port;


    public Area[] getAllArea()  {
        String uri=baseUriArea + "areas";
        System.out.println(uri);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Area[]> response = restTemplate.getForEntity(uri,Area[].class);
        Area[] areas = response.getBody();
        return areas;
    }
    public Track getTrackById(int trackId)  {
        String uri=baseUriTrack+trackId;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Track> response = restTemplate.getForEntity(uri,Track.class);
        Track track = response.getBody();
        return track;
    }
    public Track[] getAllTrack()  {
        String uri=baseUriTrack;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Track[]> response = restTemplate.getForEntity(uri,Track[].class);
        Track[] tracks = response.getBody();
        return tracks;
    }
    public double calculate_distance(Track track, Area area){
         double distance =Math.sqrt(Math.pow(track.getLatitude()-area.getAreaLatitude(),2) + Math.pow(track.getLongitude()-area.getAreaLongitude(),2));
         return distance;
    }

    public double calculate_angle(Track track, Area area){
        return Math.atan2(area.getAreaLatitude()-track.getLatitude(), area.getAreaLongitude()-track.getLongitude());
    }
    public double calculate_limit_angle(Track track, Area area){
        return Math.asin(area.getAreaRadius()/calculate_distance(track,area));
    }
    public List<AlertInfo> getAlert(Track track){
        List<AlertInfo> alertInfo = new ArrayList<AlertInfo>();

        int i = 0;
        Area[] areas = this.getAllArea();
        for (Area area : areas) {
            AlertInfo tempAlertInfo = new AlertInfo();
            tempAlertInfo.setAreaId(area.getAreaId());
            tempAlertInfo.setDistance(this.calculate_distance(track,area));
            alertInfo.add(tempAlertInfo);
        }
        return alertInfo;
    }

    public void calculate_alert(){
        Track[] tracks = this.getAllTrack();
        Area[]  areas   = this.getAllArea();
        for (Area area : areas){
            for (Track track : tracks){
                if( calculate_distance(track,area) < 10 && calculate_angle(track,area) < calculate_limit_angle(track,area) )
                {
                    System.out.println(area.getAreaId());
                    System.out.println(track.getId());
                }
            }
        }

    }
}

package c4i.vq2.alertservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlertserviceApplication {

	public static void main(String[] args)  {
		SpringApplication.run(AlertserviceApplication.class, args);
	}

}

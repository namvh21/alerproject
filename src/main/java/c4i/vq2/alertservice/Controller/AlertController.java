package c4i.vq2.alertservice.Controller;

import c4i.vq2.alertservice.Model.AlertInfo;
import c4i.vq2.alertservice.Model.Track;
import c4i.vq2.alertservice.Service.AlertService;
import c4i.vq2.alertservice.Service.SocketService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@Getter
@Setter
public class AlertController {
    @Autowired
    AlertService alertService;
    @Autowired
    SocketService socketService;

    @CrossOrigin
    @GetMapping("/alert/{id}")
    public ResponseEntity<List<AlertInfo>> apiGetAllAlert(@PathVariable int id)
    {
        try {
            Track track = alertService.getTrackById(id);
            List<AlertInfo> alertInfo = alertService.getAlert(track);
            return new ResponseEntity<>(alertInfo,HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }
}
